package guru.bitman.restitbe.core.domain;

import java.util.HashSet;
import java.util.Set;

public class User
{
    private String name;
    private Set<String> followings = new HashSet<>();

    public User(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void addFollowing(String userName)
    {
        followings.add(userName);
    }

    public Set<String> getFollowings()
    {
        return followings;
    }
}
