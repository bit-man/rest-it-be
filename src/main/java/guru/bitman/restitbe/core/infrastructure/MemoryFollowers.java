package guru.bitman.restitbe.core.infrastructure;

import guru.bitman.restitbe.core.domain.Followers;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MemoryFollowers
        implements Followers
{
    private Map<String, Set<String>> follows = new HashMap<>();

    @Override
    public void addFollower(String userName, String userNameFollowing)
    {
        Set<String> set = new HashSet<>();
        set.add(userNameFollowing);
        follows.put(userName, set);
    }

    @Override
    public Set<String> getFollows(String userName)
    {
        return follows.get(userName);
    }
}
