package guru.bitman.restitbe.core;

import guru.bitman.restitbe.core.domain.User;
import guru.bitman.restitbe.core.domain.UserAlreadyRegistered;
import guru.bitman.restitbe.core.domain.Users;
import guru.bitman.restitbe.core.infrastructure.MemoryUsers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class Iteration1
{

    private static final String PASKU_1 = "@pasku1";
    private static final String OTHER_USER = "@otherUser";

    @Test
    public void userCanRegisterWithUserNameIfNoPreviousRegistration()
            throws UserAlreadyRegistered
    {
        // Given
        final Users users = new MemoryUsers();

        // When
        User user = users.register(PASKU_1);

        // Then
        assertEquals(PASKU_1, user.getName());
    }

    @Test
    public void userCanRegisterWithUserNameIfPreviousRegistrationWithDifferentName()
            throws UserAlreadyRegistered
    {
        // Given
        final Users users = new MemoryUsers();
        users.register(OTHER_USER);

        // When
        User user = users.register(PASKU_1);

        // Then
        assertEquals(PASKU_1, user.getName());
    }

    @Test
    public void userCannotRegisterWithUserNameIfPreviouslyRegistered()
            throws UserAlreadyRegistered
    {
        // Given
        final Users users = new MemoryUsers();
        users.register(PASKU_1);

        // When
        Executable exec = () -> users.register(PASKU_1);

        // Then
        assertThrows(UserAlreadyRegistered.class, exec);
    }

    @Test
    public void userCannotRegisterWithUserNameIfPreviouslyRegisteredAndOtherRegistrationFollows()
            throws UserAlreadyRegistered
    {
        // Given
        final Users users = new MemoryUsers();
        users.register(PASKU_1);
        users.register(OTHER_USER);

        // When
        Executable exec = () -> users.register(PASKU_1);

        // Then
        assertThrows(UserAlreadyRegistered.class, exec);
    }
}
