package guru.bitman.restitbe.core.domain;

public interface Users
{
    User register(String userName)
            throws UserAlreadyRegistered;
}
