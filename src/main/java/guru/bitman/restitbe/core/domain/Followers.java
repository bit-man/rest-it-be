package guru.bitman.restitbe.core.domain;

import java.util.Set;

public interface Followers
{
    void addFollower(String userName, String userNameFollowing);

    Set<String> getFollows(String userName);
}
