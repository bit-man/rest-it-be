package guru.bitman.restitbe.core;

import guru.bitman.restitbe.core.domain.Followers;
import guru.bitman.restitbe.core.infrastructure.MemoryFollowers;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Iteration2
{

    private static final String USER_2 = "@user2";
    private static final String USER_1 = "@user1";

    @Test
    public void anyUserCanFollowAnyExistingUser()
    {
        // Given
        Followers followers = new MemoryFollowers();

        // When
        followers.addFollower(USER_1, USER_2);

        // Then
        Set<String> follows = followers.getFollows(USER_1);
        assertEquals(1, follows.size());
        assertTrue(follows.contains(USER_2));
    }
}
