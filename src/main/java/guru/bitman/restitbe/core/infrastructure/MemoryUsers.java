package guru.bitman.restitbe.core.infrastructure;

import guru.bitman.restitbe.core.domain.User;
import guru.bitman.restitbe.core.domain.UserAlreadyRegistered;
import guru.bitman.restitbe.core.domain.Users;

import java.util.HashSet;
import java.util.Set;

public class MemoryUsers
        implements Users
{
    private Set<String> registry = new HashSet<>();

    @Override
    public User register(String userName)
            throws UserAlreadyRegistered
    {
        if (registry.contains(userName))
        {
            throw new UserAlreadyRegistered();
        }

        registry.add(userName);

        return new User(userName);
    }
}
